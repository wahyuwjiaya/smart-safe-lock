#Import library
from gpiozero import LED, Button, Servo,Buzzer

#Import sub-program
import setup

lv = LED(setup.LV_PIN)

#Relay setup pin for the solenoid to work
kunci = LED(setup.RELAY_PIN)

#Button setup pin
tombol = Button(setup.BUTTON_PIN)

#LED setup pin
led_red = LED(setup.LEDRED_PIN)
led_grn = LED(setup.LEDGRN_PIN)
led_ylw = LED(setup.LEDYLW_PIN)

#Buzzer setup pin
alarm = Buzzer(setup.BUZZER_PIN)
