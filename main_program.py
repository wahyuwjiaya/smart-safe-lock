#Import library
import cv2
import telepot
import datetime
import glob
import os

from time import sleep
from telepot.loop import MessageLoop

#Import sub-program
import setup
import face

from input_output import kunci, tombol, led_red, led_grn, led_ylw, alarm, lv

CAPTURE_FILE_PREFIX = 'capture'

def action(msg):
    chat_id = msg['chat']['id']
    command = msg['text']
      
    if 'on' in command:
        message = "Menjalankan perintah "
        if 'kunci' in command:
            message = message + 'membuka kunci brankas'
            kunci.on()
            led_grn.on()
            led_red.off()
            sleep(10)
            kunci.off()
            led_grn.off()
            led_red.on()
        if 'alarm' in command:
            message = message + 'menyalakan alarm'
            alarm.beep()
            led_ylw.blink()
            led_grn.off()
            led_red.off()

        bot.sendMessage(chat_id, message)

    if 'off' in command:
        message = "Menjalankan perintah "
        if 'alarm' in command:
            message = message + 'mematikan alarm'
            alarm.off()
            led_grn.off()
            led_ylw.off()
            led_red.on()

        bot.sendMessage(chat_id, message)

bot = telepot.Bot('1620273264:AAH6DgSgo3a3pAzYEdPK787No3ggdvrq1ms')
print(bot.getMe())

MessageLoop(bot, action).run_as_thread()

if __name__ == '__main__':
    kunci.off()
    led_red.on()
    led_grn.off()
    led_ylw.off()
    alarm.off()
    lv.on()
    
    #Load training data into model.
    print('Loading training data...')
    model = cv2.face.EigenFaceRecognizer_create()
    model.read(setup.TRAINING_FILE)
    print('Training data loaded!')
    
    #Initialize camera
    camera = setup.get_camera()
    
    #Create "Capture" folder
    if not os.path.exists(setup.CAPTURE_DIR):
        os.makedirs(setup.CAPTURE_DIR)
    #Find the largest ID of existing capture images. (ex: largest files is sample3.pgm)
    #Start new images after this ID value. (ex: sample4.jpg)
    files = sorted(glob.glob(os.path.join(setup.CAPTURE_DIR, 
        CAPTURE_FILE_PREFIX + '[0-9][0-9][0-9].jpg')))
    count = 0
    if len(files) > 0:
        #Grab the count from the last filename.
        count = int(files[-1][-7:-4])+1
        
    print('Running save box...')
    print('Press button to unlock if the correct face is detected.')
    
    # TODO: Check if button is pressed.
    while True:
        if tombol.is_pressed:
            led_ylw.blink()
            print('Button pressed, looking for face...')
            
            # Check for the positive face and unlock if found.
            image = camera.read()
            filename = os.path.join(setup.CAPTURE_DIR, CAPTURE_FILE_PREFIX + '%03d.jpg' % count) 
            cv2.imwrite(filename, image)
            # Convert image to grayscale.
            image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
            # Get coordinates of single face in captured image.
            result = face.detect_single(image)
            if result is None:
                print('Could not detect single face!  Check the image in capture.pgm' \
                    ' to see what was captured and try again with only one face visible.')
                led_ylw.off()
                alarm.beep(1,1,2,False)
                bot.sendMessage(1437422852,'Ada yang mencoba membuka brankas anda pada '+str(datetime.datetime.now()))
                bot.sendPhoto(1437422852,photo=open((filename),'rb'))
                continue
            x, y, w, h = result
            # Crop and resize image to face.
            crop = face.resize(face.crop(image, x, y, w, h))
            # Test face against model.
            label, confidence = model.predict(crop)
            print('Predicted {0} face with confidence {1} (lower is more confident).'.format(
                'POSITIVE' if label == setup.POSITIVE_LABEL else 'NEGATIVE', 
                confidence))
            if label == setup.POSITIVE_LABEL and confidence < setup.POSITIVE_THRESHOLD:
                print('Recognized face!')
                led_red.off()
                led_grn.on()
                led_ylw.off()
                kunci.on()
                sleep(10)
                led_red.on()
                led_grn.off()
                kunci.off()
            else:
                print('Did not recognize face!')
                led_ylw.off()
                led_red.on()
                alarm.beep(1,1,2,False)
                bot.sendMessage(1437422852,'Ada yang mencoba membuka brankas anda pada '+str(datetime.datetime.now()))
                bot.sendPhoto(1437422852,photo=open((filename),'rb'))