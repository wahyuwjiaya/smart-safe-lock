#Import library
import glob
import os
import cv2
import sys

#Import sub-program
import setup
import face
import select
from input_output import tombol

#Prefix for positive training image filenames
POSITIVE_FILE_PREFIX = 'positive_'

def is_letter_input(letter):
    if select.select([sys.stdin,],[],[],0.0)[0]:
        input_char = sys.stdin.read(1)
        return input_char.lower() == letter.lower()
    return False 

if __name__ == '__main__':
    camera = setup.get_camera()
    #Create "positive" folder
    if not os.path.exists(setup.POSITIVE_DIR):
        os.makedirs(setup.POSITIVE_DIR)
    #Find the largest ID of existing positive images. (ex: largest files is sample3.pgm)
    #Start new images after this ID value. (ex: sample4.pgm)
    files = sorted(glob.glob(os.path.join(setup.POSITIVE_DIR, 
        POSITIVE_FILE_PREFIX + '[0-9][0-9][0-9].pgm')))
    count = 0
    if len(files) > 0:
        #Grab the count from the last filename.
        count = int(files[-1][-7:-4])+1
    print('Capturing positive training images.')
    print('Type c to capture an image.')
    print('Press Ctrl-C to quit.')
    while True:
        #Check if button pressed
        if is_letter_input('c'):   
        #if tombol.is_pressed:
                #Capture image
                print('Capturing image...')
                image = camera.read()
                #Convert image to grayscale.
                image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
                #Get coordinates of single face in captured image.
                result = face.detect_single(image)
                if result is None:
                    print('Could not detect single face!  Check the image in capture.pgm' \
                            ' to see what was captured and try again with only one face visible.')
                    continue
                x, y, w, h = result
                # Crop image as close as possible to desired face aspect ratio.
                crop = face.crop(image, x, y, w, h)
                # Save image to file.
                filename = os.path.join(setup.POSITIVE_DIR, POSITIVE_FILE_PREFIX + '%03d.pgm' % count)
                cv2.imwrite(filename, crop)
                print('Found face and wrote training image', filename)
                count += 1
